﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
	public bool hold = false;
	public bool stocked = false;
	public bool grounded = false;
	public bool attached = true;

	private bool collapsing = false;

	[SerializeField]
	float scaleForce = 0.001f;

	[SerializeField]
	float lifetime = 5;

	private Vector3 scaleFactor;

	void Start()
	{
		scaleFactor = new Vector3(-scaleForce, -scaleForce, -scaleForce);
	}

	void Update()
	{
		if (lifetime <= 0) Destroy(gameObject);

		if (collapsing)
		{
			//Peut etre changeer ça pour de la Lerp vers 0 pour éviter l'agrandissement ?
			lifetime -= 1 * Time.deltaTime;
			transform.localScale += scaleFactor * Time.deltaTime;
		}

		if (!stocked && !hold && !attached && grounded)
		{
				collapsing = true;
		}

		else
		{
			collapsing = false;
			lifetime = 5;
		}
	}
}
