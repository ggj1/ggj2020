﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{

    private GameObject Camera;

    private void Start()
    {
        Camera = GameObject.FindGameObjectWithTag("MainCamera").gameObject;
    }

    void Update()
    {
        transform.LookAt(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f));
    }
}
