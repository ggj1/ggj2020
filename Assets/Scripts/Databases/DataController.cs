﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;                                                        // The System.IO namespace contains functions related to loading and saving files

public class DataController : MonoBehaviour
{
    public void PlayerPrefSave()
    {
        PlayerPrefs.Save();
    }
    public bool isPlayerPref(string name)
    {
        return PlayerPrefs.HasKey(name);
    }
    public void PlayerPrefDeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }
    public void PlayerPrefSetInt(string name, int number)
    {
        PlayerPrefs.SetInt(name, number);
    }
    public int PlayerPrefGetInt(string name)
    {
        return PlayerPrefs.GetInt(name);
    }
    public void PlayerPrefSetFloat(string name, float number)
    {
        PlayerPrefs.SetFloat(name, number);
    }
    public float PlayerPrefGetFloat(string name)
    {
        return PlayerPrefs.GetFloat(name);
    }
    public void PlayerPrefSetString(string name, string stringValue)
    {
        PlayerPrefs.SetString(name, stringValue);
    }
    public string PlayerPrefGetString(string name)
    {
        return PlayerPrefs.GetString(name);
    }
}