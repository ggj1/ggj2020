﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgress : MonoBehaviour
{
    public DataController dataController;
    public int highestScore = 0;
    public string playerName1 = "Noob1";
    public string playerName2 = "Noob2";
    public Color32 SettingBackground;
    public int SettingPostProcessing;
    public int Money = 0;
    public string playerControls;

    public void Start()
    {
        GetAllProgress();
        Debug.Log("PlayerControls = " + playerControls);
    }
    void GetAllProgress()
    {
        if (dataController.isPlayerPref("HighScore")) { highestScore = dataController.PlayerPrefGetInt("HighScore"); }
        if (dataController.isPlayerPref("PlayerName1")) { playerName1 = dataController.PlayerPrefGetString("PlayerName1"); }
        if (dataController.isPlayerPref("PlayerName2")) { playerName2 = dataController.PlayerPrefGetString("PlayerName2"); }
        if (dataController.isPlayerPref("SettingBackgroundColor"))
        {
            string backGroundString = dataController.PlayerPrefGetString("SettingBackgroundColor");
            string[] backGroundStringArray = backGroundString.Split(char.Parse(" "));
            SettingBackground = new Color32(byte.Parse(backGroundStringArray[0]), byte.Parse(backGroundStringArray[1]), byte.Parse(backGroundStringArray[2]), byte.Parse(backGroundStringArray[3]));
        }
        if (dataController.isPlayerPref("SettingPostProcessing")) { SettingPostProcessing = dataController.PlayerPrefGetInt("SettingPostProcessing"); }
        if (dataController.isPlayerPref("Money")) { Money = dataController.PlayerPrefGetInt("Money"); }
        if (dataController.isPlayerPref("playerControls")) { playerControls = dataController.PlayerPrefGetString("playerControls"); }
    }
    public void getPlayerControls()
    {
        if (dataController.isPlayerPref("playerControls")) { playerControls = dataController.PlayerPrefGetString("playerControls"); }
    }
    public void saveMoney(int number)
    {
        dataController.PlayerPrefSetInt("Money", number);
    }
    public void saveSettingBackground(byte r, byte g, byte b, byte a)
    {
        dataController.PlayerPrefSetString("SettingBackgroundColor", r + " " + g + " " + b + " " + a);
    }
    public void saveSettingPostProcessing(int number)
    {
        dataController.PlayerPrefSetInt("SettingPostProcessing", number);
    }
    public void saveHighScore(int number)
    {
        dataController.PlayerPrefSetInt("HighScore", number);
    }
    public void saveplayerControls(string controls)
    {
        Debug.Log("putting" + controls + " into playerControls");
        dataController.PlayerPrefSetString("playerControls", controls);
    }
}
