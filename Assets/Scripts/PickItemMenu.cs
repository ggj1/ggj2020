﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickItemMenu : MonoBehaviour
{
	private int cursorPos = 0;
	private Vector3 offset = new Vector3(3.5f, 0, 0);
	public Transform cursor;
	public Transform ItemHolder1;
	public Transform ItemHolder2;
	public Transform ItemHolder3;
	public GameObject activePlayer;

	private InputManager im;

	private void Start()
	{
		im = GameObject.Find("InputManager").GetComponent<InputManager>();
	}

	void Update()
    {
		if (activePlayer.tag == "Player1")
		{
			if (Input.GetKeyDown(im.Right1) && cursorPos > -1)
			{
				cursorPos -= 1;
				cursor.localPosition -= offset;

			}
			else if (Input.GetKeyDown(im.Left1) && cursorPos < 1)
			{
				cursorPos += 1;
				cursor.localPosition += offset;
			}

			if (Input.GetKeyDown(im.ActionOne1))
			{
				Grab();
			}

			if (Input.GetKeyDown(im.ActionTwo1))
			{
				enabled = false;
				activePlayer.GetComponent<ControllerChara>().enabled = true;
			}
		}
		else
		{
			if (Input.GetKeyDown(im.Right2) && cursorPos > -1)
			{
				cursorPos -= 1;
				cursor.localPosition -= offset;

			}
			else if (Input.GetKeyDown(im.Left2) && cursorPos < 1)
			{
				cursorPos += 1;
				cursor.localPosition += offset;
			}

			if (Input.GetKeyDown(im.ActionOne2))
			{
				Grab();
			}

			if (Input.GetKeyDown(im.ActionTwo2))
			{
				enabled = false;
				activePlayer.GetComponent<ControllerChara>().enabled = true;
			}
		}
    }

	void Grab ()
	{
		if (cursorPos == -1)
		{
			// Assign Slot1 item to ItemHolder on player
			activePlayer.GetComponentInChildren<GrabDrop>().objectHold = ItemHolder1.GetChild(0).gameObject;

			//Change the transform to the active player
			ItemHolder1.GetChild(0).SetParent(activePlayer.transform);
		
			//Delete the 2 others objects
			Destroy(ItemHolder2.GetChild(0).gameObject);
			Destroy(ItemHolder3.GetChild(0).gameObject);
		}
		if (cursorPos == 0)
		{
			activePlayer.GetComponentInChildren<GrabDrop>().objectHold = ItemHolder2.GetChild(0).gameObject;

			//Change the transform to the active player
			ItemHolder2.GetChild(0).SetParent(activePlayer.transform);

			//Delete the 2 others objects
			Destroy(ItemHolder1.GetChild(0).gameObject);
			Destroy(ItemHolder3.GetChild(0).gameObject);
		}
		if (cursorPos == 1)
		{
			activePlayer.GetComponentInChildren<GrabDrop>().objectHold = ItemHolder3.GetChild(0).gameObject;

			//Change the transform to the active player
			ItemHolder3.GetChild(0).SetParent(activePlayer.transform);

			//Delete the 2 others objects
			Destroy(ItemHolder1.GetChild(0).gameObject);
			Destroy(ItemHolder2.GetChild(0).gameObject);
		}

		enabled = false;
		activePlayer.GetComponentInChildren<Item>().attached = false;
		activePlayer.GetComponentInChildren<Item>().hold = true;
		activePlayer.GetComponentInChildren<GrabDrop>().isHolding = true;
		activePlayer.GetComponent<ControllerChara>().enabled = true;
	}
}
