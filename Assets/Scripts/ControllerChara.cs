﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]


public class ControllerChara : MonoBehaviour
{
	public int speed = 2;

	//public KeyCode haut;
	//public KeyCode bas;
	//public KeyCode gauche;
	//public KeyCode droit;
	//public KeyCode dialogueTest;
	//public KeyCode grab;
	//public KeyCode drop;

	public bool isPlayer1;

	public InputManager im;

	public bool holding = false;
	Rigidbody rigidbody;
	BoxCollider hitbox;
	public GameObject Body;
	Animator animator;
	SpriteRenderer spriteRenderer;

	private void Start()
	{
		im = GameObject.Find("InputManager").GetComponent<InputManager>();
		rigidbody = gameObject.GetComponent<Rigidbody>();
		hitbox = gameObject.GetComponent<BoxCollider>();
		animator = Body.GetComponent<Animator>();
		spriteRenderer = Body.GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void LateUpdate()
	{
		if (isPlayer1)
		{
			if (Input.GetKey(im.Up1))
			{
				transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
				animator.SetBool("back", true);
				animator.SetBool("front", false);
				animator.SetBool("side", false);
				//rigidbody.AddForce(Vector2.up * speed);
			}

			if (Input.GetKey(im.Left1))
			{
				transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
				animator.SetBool("side", true);
				animator.SetBool("back", false);
				animator.SetBool("front", false);

				spriteRenderer.flipX = true;
			}

			if (Input.GetKey(im.Down1))
			{
				transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
				animator.SetBool("front", true);
				animator.SetBool("back", false);
				animator.SetBool("side", false);
			}

			if (Input.GetKey(im.Right1))
			{
				transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
				animator.SetBool("side", true);
				animator.SetBool("back", false);
				animator.SetBool("front", false);
				spriteRenderer.flipX = false;
			}

			if(!Input.GetKey(im.Up1) && !Input.GetKey(im.Left1) && !Input.GetKey(im.Down1) && !Input.GetKey(im.Right1))
			{
				animator.SetBool("side", false);
				animator.SetBool("back", false);
				animator.SetBool("front", false);
			}

			if (Input.GetKey(KeyCode.W))
			{
				TextMeshPro tmp = transform.GetComponentInChildren<TextMeshPro>();
				tmp.text = "hello there1";
			}
		}
		else
		{
			if (Input.GetKey(im.Up2))
			{
				transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
				//rigidbody.AddForce(Vector2.up * speed);1
				animator.SetBool("back", true);
				animator.SetBool("front", false);
				animator.SetBool("side", false);
			}

			if (Input.GetKey(im.Left2))
			{
				transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
				animator.SetBool("side", true);
				animator.SetBool("back", false);
				animator.SetBool("front", false);

				spriteRenderer.flipX = true;
			}

			if (Input.GetKey(im.Down2))
			{
				transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
				animator.SetBool("front", true);
				animator.SetBool("back", false);
				animator.SetBool("side", false);
			}

			if (Input.GetKey(im.Right2))
			{
				transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
				animator.SetBool("side", true);
				animator.SetBool("back", false);
				animator.SetBool("front", false);
				animator.SetBool("hold", false);
				animator.SetBool("drop", false);
				spriteRenderer.flipX = false;
			}
			if (!Input.GetKey(im.Up2) && !Input.GetKey(im.Left2) && !Input.GetKey(im.Down2) && !Input.GetKey(im.Right2))
			{
				animator.SetBool("side", false);
				animator.SetBool("back", false);
				animator.SetBool("front", false);
			}


			if (Input.GetKey(KeyCode.X))
			{
				TextMeshPro tmp = transform.GetComponentInChildren<TextMeshPro>();
				tmp.text = "hello there2";
			}
		}
	}
}
