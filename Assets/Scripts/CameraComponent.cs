﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : MonoBehaviour
{

    public GameObject CameraDistortion;

    public void ActiveDistortion()
    {
        CameraDistortion.SetActive(true);
    }

    public void DeactiveDistortion()
    {
        CameraDistortion.SetActive(false);
    }

}
