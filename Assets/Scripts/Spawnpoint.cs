﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the Spawner class.
/// </summary>
public class Spawnpoint : MonoBehaviour
{
	int id = 0;
    /// <summary>
    /// Seconds until the gameobject spawn again
    /// </summary>
    public float delay = 1f;
    /// <summary>
    /// Is the spawn automatic
    /// </summary>
    public bool isSpawnAuto = false;
    /// <summary>
    /// String of the gameobject to be spawned, must be under a resources folder
    /// </summary>
    public string resourcesName;
    public GameManager gm;

    /// <summary>
    /// Trigger on enable
    /// </summary>
    private void OnEnable()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        StartCoroutine(Countdown(delay));
    }

    /// <summary>
    /// Make a countdown to trigger spawn function every passed float time
    /// </summary>
    /// <param name="ytime">The amount of float time before spawn function is called</param>
    private IEnumerator Countdown(float time)
    {
        yield return new WaitForSeconds(time);
        if (!gm.isMenuAff)
        {
            if (isSpawnAuto)
            {
                spawn();
            }
        }
        if(isSpawnAuto)
        {
            StartCoroutine(Countdown(delay));
        }
    }

    /// <summary>
    /// Responsible for spawning a gameobject under a resources folder
    /// </summary>
    public void spawn()
    {
        GameObject soldier = Instantiate((GameObject)Resources.Load(resourcesName, typeof(GameObject)), getRandomPosition(), Quaternion.identity, null);
		soldier.name = resourcesName + "_" + id;
		id++;
    }

    /// <summary>
    /// Responsible for returning a random transform position under the actual gameobject scale
    /// </summary>
    /// <returns>A random position withing the Spawner range as a Vector3</returns>
    public Vector3 getRandomPosition()
    {
        return new Vector3(
            Random.Range(transform.position.x - (gameObject.transform.localScale.x / 2), transform.position.x + (gameObject.transform.localScale.x / 2)),
            Random.Range(transform.position.y - (gameObject.transform.localScale.y / 2), transform.position.y + (gameObject.transform.localScale.y / 2)),
            Random.Range(transform.position.z - (gameObject.transform.localScale.z / 2), transform.position.z + (gameObject.transform.localScale.z / 2))
            );
    }
}
