﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperationTable : MonoBehaviour
{
    public GameManager gm;
    public bool isPlayer1;
    public Spawnpoint spawner;

	public GameObject c;
	public GameObject b;
	public GameObject y;
	public GameObject j;


	void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if((other.gameObject.tag == "Player1" && isPlayer1) || (other.gameObject.tag == "Player2" && !isPlayer1))
        {
            Debug.Log("Player 0n the operation table");
            if(FindComponentInChildWithTag(other.gameObject, "Item") != null)
            {
				
                createSoldier(FindComponentInChildWithTag(other.gameObject, "Item"));

				ajoutBodyPart(FindComponentInChildWithTag(other.gameObject, "Item"));

			}
        }
    }

    public void createSoldier(GameObject itemPart)
    {
        switch(itemPart.name)
        {
            case "Leg":
                Debug.Log("Creating a Leg Soldier");
                if(isPlayer1)
                {
                    spawner.resourcesName = "SoldierLegTeamOne";
                }
                else
                {
                    spawner.resourcesName = "SoldierLegTeamTwo";
                }
                spawner.spawn();
                break;
            case "Eye":
                Debug.Log("Creating a Eye Soldier");
                if (isPlayer1)
                {
                    spawner.resourcesName = "SoldierEyeTeamOne";
                }
                else
                {
                    spawner.resourcesName = "SoldierEyeTeamTwo";
                }
                spawner.spawn();
                break;
            case "Heart":
                Debug.Log("Creating a Heart Soldier");
                if (isPlayer1)
                {
                    spawner.resourcesName = "SoldierHeartTeamOne";
                }
                else
                {
                    spawner.resourcesName = "SoldierHeartTeamTwo";
                }
                spawner.spawn();
                break;
            default:
                if (isPlayer1)
                {
                    spawner.resourcesName = "SoldierTeamOne";
                }
                else
                {
                    spawner.resourcesName = "SoldierTeamTwo";
                }
                spawner.spawn();
                break;
        }

        Destroy(itemPart);
    }

    public GameObject FindComponentInChildWithTag(GameObject parent, string tag)
    {
        Transform t = parent.transform;
        foreach (Transform tr in t)
        {
            if (tr.tag == tag)
            {
                return tr.gameObject;
            }
        }
        return null;
    }

    void ajoutBodyPart(GameObject bp)
	{
		switch (bp.name)
		{
			case "Leg":
				j = bp;
				break;

			case "eye":
				y = bp;
				break;

			case "Heart":
				c = bp;
				createLife();
				break;

			case "Arm":
				b = bp;
				break;
		}
	}

	void createLife()
	{
		if (j != null && y != null && b != null)
		{
			spawner.resourcesName = "8";
		}
		else if (j != null && b == null)
		{
			spawner.resourcesName = "7";
		}
		else if (j != null && y == null)
		{
			spawner.resourcesName = "6";
		}
		else if (y != null && b == null)
		{
			spawner.resourcesName = "5";
		}
		else if (j == null)
		{
			spawner.resourcesName = "4";
		}
		else if (b == null)
		{
			spawner.resourcesName = "3";
		}
		else if (y == null)
		{
			spawner.resourcesName = "2";
		}
		else
		{
			spawner.resourcesName = "1";
		}
	}

}
