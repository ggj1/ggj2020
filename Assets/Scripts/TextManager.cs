﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextManager : MonoBehaviour
{
	public List<string> ramasseOrg;
	public List<string> creerSold;
	public List<string> tuerSold ;

    // Start is called before the first frame update
    void Start()
    {
		ramasseOrg.Add("T’inquiète pas pour ton œil, je vais l’envoyer en orbite !");
		ramasseOrg.Add("Pour ce genre d’opérations, contrairement à toi, j’ai l’oeil !");

		ramasseOrg.Add("Pour ce bras, j’aurais besoin d’un peu d’huile de coude !");
		ramasseOrg.Add("Tu fais les petits bras durant cette guerre !");

		ramasseOrg.Add("Je pensais pas à ça quand je parlais de 'fourmis dans les pieds'.");
		ramasseOrg.Add("Maintenant, je peux faire un pied de nez !");

		ramasseOrg.Add("Je suis quelqu’un qui a souvent le coeur sur la main.");
		ramasseOrg.Add("Avoir cet organe me tenait à coeur.");

	}

	void ramasserOrgane(string organe)
	{
		int idRandom = (int)(Random.value * 1);
		Debug.Log("intRadom = "+idRandom);
		TextMeshPro tmp;
		string returnValue;

		switch (organe)
		{
			case "oeil":
				returnValue = ramasseOrg[idRandom];
			break;

			case "bras":
				returnValue = ramasseOrg[idRandom +2];
				break;

			case "jambe":
				returnValue = ramasseOrg[idRandom +4];
				break;

			case "coeur":
				returnValue = ramasseOrg[idRandom +6];
				break;

			
			default:
			break;
		}
		//tmp.text = returnValue;
		//TODO
	}

	void creerSoldat(string soldat)
	{
		int idRandom = (int)(Random.value * 1);
		Debug.Log("intRadom = " + idRandom);
		TextMeshPro tmp;
		string returnValue;

		switch (soldat)
		{
			case "oeil":
				returnValue = creerSold[idRandom];
				break;

			case "bras":
				returnValue = creerSold[idRandom + 2];
				break;

			case "jambe":
				returnValue = creerSold[idRandom + 4];
				break;

			case "coeur":
				returnValue = creerSold[idRandom + 6];
				break;


			default:
				break;
		}
		//tmp.text = returnValue;
		//TODO
	}

	void tuerSoldat(string mort)
	{
		int idRandom = (int)(Random.value * 1);
		Debug.Log("intRadom = " + idRandom);
		TextMeshPro tmp;
		string returnValue;

		switch (mort)
		{
			case "oeil":
				returnValue = tuerSold[idRandom];
				break;

			case "bras":
				returnValue = tuerSold[idRandom + 2];
				break;

			case "jambe":
				returnValue = tuerSold[idRandom + 4];
				break;

			case "coeur":
				returnValue = tuerSold[idRandom + 6];
				break;


			default:
				break;
		}
		//tmp.text = returnValue;
		//TODO
	}
}
