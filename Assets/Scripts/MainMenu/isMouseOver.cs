﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class isMouseOver : MonoBehaviour
{
    public bool isMouseOverThisElementBool = false;
    public bool isMouseOverThisElement()
    {
        return isMouseOverThisElementBool = EventSystem.current.IsPointerOverGameObject();
    }

}