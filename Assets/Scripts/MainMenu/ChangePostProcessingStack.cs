﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class ChangePostProcessingStack : MonoBehaviour
{

    public GameObject Camera;
    public PostProcessingProfile postProcessingProfile;

    private void Start()
    {
        Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().gameObject;
    }

    public void ChangePostProcessing()
    {
        Camera.GetComponent<PostProcessingBehaviour>().profile = postProcessingProfile;
    }
}
