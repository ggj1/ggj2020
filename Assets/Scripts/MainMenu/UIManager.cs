﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public MenuController mouseManager;
    public TextMeshProUGUI BestScoreUI;
    public TextMeshProUGUI MoneyUI;
    public PlayerProgress playerProgress;
    public Color32 UIDefault;
    public Color32 UIHover;
    public Color32 UISelected;
    public Color32 UIBar;
    public Color32 UIBarHover;
    public Color32 UIBarSelected;
    public Color32 UIShopHoverSelected;
    public Color32 UIShopHoverUnlocked;
    public Color32 UIShopHoverLocked;
    public Color32 UIShopDefaultSelected;
    public Color32 UIShopDefaultUnlocked;
    public Color32 UIShopDefaultLocked;
    public string OverMainMenuName = "";
    public string MainMenuName = "";
    public string MainMenuNameShop = "";
    public string MainSubMenuName = "";
    public string BarImageName = "Image";

    public string MenuName = "MainMenu";
    public string SubMenuName = "CanvasSubMenu";
    public string SubSubMenuName = "CanvasSubSubMenu";

    public GameObject SelectedMainMenu;
    public GameObject SelectedMenu;
    public GameObject SelectedSubMenu;

    public GameObject OpenedMenu;
    public GameObject OpenedSubMenu;
    public GameObject OpenedSubSubMenu;

    public GameObject[] SubSubMenus;
    public GameObject[] SubMenus;
    public GameObject[] MainMenus;
    public void Start()
    {
        playerProgress = GameObject.Find("SaveManager").GetComponent<PlayerProgress>();
        //setBestScoreUI(playerProgress.highestScore);
        //setMoneyUI(playerProgress.Money);
        if (SubSubMenus.Length > 0)
        {
            foreach (GameObject SubSubMenu in SubSubMenus)
            {
                SubSubMenu.SetActive(false);
            }
        }
        if (SubMenus.Length > 0)
        {
            foreach (GameObject SubMenu in SubMenus)
            {
                SubMenu.SetActive(false);
            }
        }
        if (MainMenus.Length > 0)
        {
            foreach (GameObject MainMenu in MainMenus)
            {
                MainMenu.SetActive(false);
            }
        }
    }
    public void setMoneyUI(int number)
    {
        MoneyUI.text = "" + number;
    }
    public void setBestScoreUI(int bestScore)
    {
        BestScoreUI.text = "" + bestScore;
    }
    void FixedUpdate()
    {
        RaycastUI();
    }
    public void RaycastUI()
    {
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        pointer.position = mouseManager.CursorPosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);

        if (raycastResults.Count > 0)
        {
            foreach (var go in raycastResults)
            {
                if (go.gameObject.CompareTag(OverMainMenuName))
                {
                    if (SelectedMainMenu != go.gameObject)
                    {
                        //Since we are hovering a OverMainMenu, we want to close every opened submenu
                        if (OpenedSubMenu)
                            ResetOldUIMenu();

                        //Since we are hovering a MainMenu, we want to close every opened subsubmenu
                        if (OpenedSubSubMenu)
                            ResetOldUISubMenu();
                        //ResetOldUIMainMenu();
                        //HoveringUIMenu(go.gameObject);
                        SelectedMainMenu = go.gameObject;
                        OpenMenu(SelectedMainMenu);
                    }
                }
                else if (go.gameObject.CompareTag(MainMenuName))
                {
                    if (SelectedMenu != go.gameObject)
                    {
                        //Since we are hovering a MainMenu, we want to close every opened subsubmenu
                        if (OpenedSubSubMenu)
                        {
                            ResetOldUISubMenu();
                        }
                        ResetOldUIMenu();
                        HoveringUIMenu(go.gameObject);
                        SelectedMenu = go.gameObject;
                        OpenSubMenu(SelectedMenu);
                    }
                }
                else if(go.gameObject.CompareTag(MainMenuNameShop))
                {
                    if (SelectedMenu != go.gameObject)
                    {
                        //Since we are hovering a MainMenu, we want to close every opened subsubmenu
                        if (OpenedSubSubMenu)
                        {
                            ResetOldUISubMenu();
                        }
                        Debug.Log(go.gameObject.name);
                        ResetOldUIMenuShop();
                        HoveringUIMenuShop(go.gameObject);
                        SelectedMenu = go.gameObject;
                        OpenSubMenu(SelectedMenu);
                    }
                }
                else if (go.gameObject.CompareTag(MainSubMenuName))
                {
                    if (SelectedSubMenu != go.gameObject)
                    {
                        ResetOldUISubMenu();
                        HoveringUIMenu(go.gameObject);
                        SelectedSubMenu = go.gameObject;
                        OpenSubSubMenu(SelectedSubMenu);
                    }
                }
            }
        }
    }

    public void ResetOldUIMainMenu()
    {
        if (SelectedMainMenu)
        {
            if (SelectedMainMenu.GetComponent<Image>())
            {
                SelectedMainMenu.GetComponent<Image>().color = UIDefault;
                if (SelectedMainMenu.transform.Find(BarImageName))
                    SelectedMainMenu.transform.Find(BarImageName).gameObject.GetComponent<Image>().color = UIBar;
            }
        }
    }

    public void ResetOldUIMenu()
    {
        if (SelectedMenu && !SelectedMenu.CompareTag(MainMenuNameShop))
        {
            if (SelectedMenu.GetComponent<Image>())
            {
                SelectedMenu.GetComponent<Image>().color = UIDefault;
                if (SelectedMenu.transform.Find(BarImageName))
                    SelectedMenu.transform.Find(BarImageName).gameObject.GetComponent<Image>().color = UIBar;
            }
        }
    }
    public void ResetOldUIMenuShop()
    {
        if (SelectedMenu && SelectedMenu.CompareTag(MainMenuNameShop))
        {
            if (SelectedMenu.GetComponent<Image>())
            {
                if (SelectedMenu.transform.Find(BarImageName))
                    SelectedMenu.transform.Find(BarImageName).gameObject.GetComponent<Image>().color = UIBar;
            }
        }
    }
    public void ResetOldUISubMenu()
    {
        if (SelectedSubMenu)
        {
            if (SelectedSubMenu.GetComponent<Image>())
            {
                SelectedSubMenu.GetComponent<Image>().color = UIDefault;
                if (SelectedSubMenu.transform.Find(BarImageName))
                    SelectedSubMenu.transform.Find(BarImageName).gameObject.GetComponent<Image>().color = UIBar;
            }
        }
    }

    public void HoveringUIMenu(GameObject UI)
    {
        if (UI.GetComponent<Image>())
        {
            UI.GetComponent<Image>().color = UIHover;
            if (UI.transform.Find(BarImageName))
                UI.transform.Find(BarImageName).gameObject.GetComponent<Image>().color = UIBarHover;
        }
    }
    public void HoveringUIMenuShop(GameObject UI)
    {
        if (UI.GetComponent<Image>())
        {
            if (UI.transform.Find(BarImageName))
                UI.transform.Find(BarImageName).gameObject.GetComponent<Image>().color = UIBarHover;
        }
    }

    public void CloseOpenedSubMenu(GameObject SubMenu)
    {
        SubMenu.SetActive(false);
    }

    public void CoolAnimationOpening(GameObject SubMenu)
    {
        SubMenu.GetComponent<Animation>().Play();
    }

    public void OpenMenu(GameObject ParentOverMainMenu)
    {
        if (ParentOverMainMenu.GetComponent<SubMenus>())
        {
            if (ParentOverMainMenu.GetComponent<SubMenus>().SubMenu)
            {
                GameObject MainMenu = ParentOverMainMenu.GetComponent<SubMenus>().SubMenu;
                if (OpenedMenu != MainMenu)
                {
                    if (OpenedMenu != null)
                        CloseOpenedSubMenu(OpenedMenu);

                    OpenedMenu = MainMenu;
                    MainMenu.SetActive(true);
                    CoolAnimationOpening(MainMenu);
                }
            }
        }
    }

    public void OpenSubMenu(GameObject ParentMainMenu)
    {
        if (ParentMainMenu.GetComponent<SubMenus>())
        {
            if (ParentMainMenu.GetComponent<SubMenus>().SubMenu)
            {
                GameObject SubMenu = ParentMainMenu.GetComponent<SubMenus>().SubMenu;
                if (OpenedSubMenu != SubMenu)
                {
                    if (OpenedSubMenu != null)
                        CloseOpenedSubMenu(OpenedSubMenu);

                    OpenedSubMenu = SubMenu;
                    SubMenu.SetActive(true);
                    CoolAnimationOpening(SubMenu);
                }
            }
        }
    }

    public void OpenSubSubMenu(GameObject ParentSubMenu)
    {
        if (ParentSubMenu.GetComponent<SubMenus>())
        {
            if (ParentSubMenu.GetComponent<SubMenus>().SubMenu)
            {
                GameObject SubSubMenu = ParentSubMenu.GetComponent<SubMenus>().SubMenu;
                if (OpenedSubMenu != SubSubMenu)
                {
                    if (OpenedSubSubMenu != null)
                        CloseOpenedSubMenu(OpenedSubSubMenu);

                    OpenedSubSubMenu = SubSubMenu;
                    SubSubMenu.SetActive(true);
                    CoolAnimationOpening(SubSubMenu);
                }
            }
        }
    }

}
