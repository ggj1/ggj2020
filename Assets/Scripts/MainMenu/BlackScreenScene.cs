﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackScreenScene : MonoBehaviour
{

    private void Start()
    {
        screenFadeOut();
    }
    private IEnumerator changeBlackTransparency(float startColor, float endColor, float time)
    {
        float elapsedTime = 0;
        while (elapsedTime < time)
        {
            GetComponent<Image>().color = new Color32(0, 0, 0, (byte) Mathf.Lerp(startColor, endColor, elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    public void screenFadeIn()
    {
        StartCoroutine(changeBlackTransparency(0, 255, 1));
    }
    public void screenFadeOut()
    {
        StartCoroutine(changeBlackTransparency(255, 0, 1));
    }
}
