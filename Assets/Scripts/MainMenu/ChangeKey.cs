﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeKey : MonoBehaviour
{
    public InputManager inputManager;
    public string keycodVariableName;
    public TMP_Text buttonText;

    public bool isWaitingForKey = false;

    private void Start()
    {
        inputManager = GameObject.Find("InputManager").GetComponent<InputManager>();
        Debug.Log("Change key Variable name : " + keycodVariableName);
        changeButtonText("KEY : " + inputManager.getKey(keycodVariableName));
    }

    public void changeKey()
    {
        Debug.Log("changeKey " + keycodVariableName);
        changeButtonText("PRESS A KEY");
        isWaitingForKey = true;
    }

    public void changeButtonText(string text)
    {
        buttonText.text = text;
    }

    public void OnGUI()
    {
        if (isWaitingForKey)
        {
            Event e = Event.current;

            if (e.type == EventType.KeyDown)
            {
                Debug.Log("Detected key code: " + e.keyCode);
                inputManager.assignKey(keycodVariableName, e.keyCode);
                isWaitingForKey = false;
                StartCoroutine(waitForEndFrameToChangeButton(e.keyCode.ToString()));
            }
        }
    }

    public IEnumerator waitForEndFrameToChangeButton(string keycode)
    {
        yield return new WaitForEndOfFrame();
        changeButtonText("Key : " + keycode);
    }
}
