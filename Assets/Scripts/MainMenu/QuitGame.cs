﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{

    public void quitGame()
    {
        Debug.Log("Quit game pressed");
        Application.Quit();
    }
}
