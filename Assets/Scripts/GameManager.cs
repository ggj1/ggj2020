﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public List<GameObject> TeamOne = new List<GameObject>();
    public List<GameObject> TeamTwo = new List<GameObject>();

	public GameObject menu;
	public bool isMenuAff;

	[Header("Battle")]
	public int lifeJ1 = 3;
	public int lifeJ2 = 3;

	private void Start()
	{
		StartCoroutine(fadeIn());
        pauseTemporel();
    }

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			pauseTemporel();
		}
	}

	public void pauseTemporel()
	{
        isMenuAff = true;
        menu.SetActive(true);
	}

	public void lectureTemporel()
	{
        isMenuAff = false;
        menu.SetActive(false);
	}

	public void addToListTeamOne(GameObject teamOne)
    {
        TeamOne.Add(teamOne);
    }
    public void addToListTeamTwo(GameObject teamTwo)
    {
        TeamTwo.Add(teamTwo);
    }

    public void removeToListTeamOne(GameObject teamOne)
    {
        TeamOne.Remove(teamOne);
        teamOne.GetComponent<BasicSoldierAI>().killSelf();
    }
    public void removeToListTeamTwo(GameObject teamTwo)
    {
        TeamTwo.Remove(teamTwo);
        teamTwo.GetComponent<BasicSoldierAI>().killSelf();
    }

    public void trackMe(GameObject tracked)
    {
        switch (tracked.tag)
        {
            case "TeamOne":
                addToListTeamOne(tracked);
                break;
            case "TeamTwo":
                addToListTeamTwo(tracked);
                break;
            default:
                print("Untracked gameobject");
                break;
        }
    }

    public void resetGame()
    {
        Debug.Log("Reseting");
		SceneManager.LoadScene(0);
		//Call save manager to load player pref.
	}

	public IEnumerator fadeIn()
	{
		yield return null;
	}

	public IEnumerator fadeOut()
	{
		yield return null;
	}

	public void verifLife(int team)
	{
		if (team == 1)
		{
			if (lifeJ1 - 1 <= 0)
			{
				pauseTemporel();
				Debug.Log("J2 WIN");
			}
			else
			{
				lifeJ1--;
			}
		}

		if (team == 2)
		{
			if (lifeJ2 - 1 <= 0)
			{
				pauseTemporel();
				Debug.Log("J1 WIN");
			}
			else
			{
				lifeJ2--;
			}
		}
	}

}
