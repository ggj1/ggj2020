﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nexusScript : MonoBehaviour
{
	public string tagtag;
	public GameManager gm;

	void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject.tag == tagtag)
		{
			if (tagtag == "TeamOne")
			{
				gm.TeamOne.Remove(collision.gameObject);
			}
			else if (tagtag == "TeamTwo")
			{
				gm.TeamTwo.Remove(collision.gameObject);
			}
			else
			{
				Debug.Log("hmm something went wrong...");
			}
			Destroy(collision.gameObject);
		}
	}
}
