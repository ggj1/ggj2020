﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    private PlayerProgress playerProgress;

    public KeyCode Up1;
    public KeyCode Up2;
    public KeyCode Down1;
    public KeyCode Down2;
    public KeyCode Left1;
    public KeyCode Left2;
    public KeyCode Right1;
    public KeyCode Right2;
    public KeyCode ActionOne1;
    public KeyCode ActionOne2;
    public KeyCode ActionTwo1;
    public KeyCode ActionTwo2;


    void Start()
    {
        playerProgress = GameObject.Find("SaveManager").GetComponent<PlayerProgress>();

        updateInputs();
    }

    public void updateInputs()
    {
        decodeControls(playerProgress.playerControls);
    }

    public void loadDefault()
    {
        Up1 = KeyCode.Z;
        Down1 = KeyCode.S;
        Right1 = KeyCode.D;
        Left1 = KeyCode.Q;
        Up2 = KeyCode.UpArrow;
        Down2 = KeyCode.DownArrow;
        Right2 = KeyCode.RightArrow;
        Left2 = KeyCode.LeftArrow;
        ActionOne1 = KeyCode.A;
        ActionOne2 = KeyCode.RightShift;
        ActionTwo1 = KeyCode.E;
        ActionTwo2 = KeyCode.RightControl;
    }

    public void decodeControls(string controls)
    {
        playerProgress.getPlayerControls();
        Debug.Log("decodeControls");
        string[] playerInputs = controls.Split('|');
        string player1Input = playerInputs[0];
        string player2Input = playerInputs[1];

        string[] player1Keys = player1Input.Split(',');
        string[] player2Keys = player2Input.Split(',');

        Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player1Keys[0]);
        Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player1Keys[1]);
        Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player1Keys[2]);
        Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player1Keys[3]);
        Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player2Keys[0]);
        Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player2Keys[1]);
        Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player2Keys[2]);
        Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player2Keys[3]);
        ActionOne1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player1Keys[4]);
        ActionOne2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player2Keys[4]);
        ActionTwo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player1Keys[5]);
        ActionTwo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), player2Keys[5]);
    }

    public void encodeControls()
    {
        string encoded = Up1.ToString() + "," + Left1.ToString() + "," + Down1.ToString() + "," + Right1.ToString() + "," + ActionOne1.ToString() + "," + ActionTwo1.ToString() + "|" +
            Up2.ToString() + "," + Down2.ToString() + "," + Right2.ToString() + "," + Left2.ToString() + "," + ActionOne2.ToString() + "," + ActionTwo2.ToString();

        Debug.Log("encoded = "+ encoded);
        playerProgress.saveplayerControls(encoded);
    }

    public void assignKey(string name, KeyCode keyCode)
    {
        switch(name)
        {
            case "Up1":
                Up1 = keyCode;
                break;
            case "Left1":
                Left1 = keyCode;
                break;
            case "Down1":
                Down1 = keyCode;
                break;
            case "Right1":
                Right1 = keyCode;
                break;
            case "ActionOne1":
                ActionOne1 = keyCode;
                break;
            case "ActionTwo1":
                ActionTwo1 = keyCode;
                break;
            case "Up2":
                Up2 = keyCode;
                break;
            case "Left2":
                Left2 = keyCode;
                break;
            case "Down2":
                Down2 = keyCode;
                break;
            case "Right2":
                Right2 = keyCode;
                break;
            case "ActionOne2":
                ActionOne2 = keyCode;
                break;
            case "ActionTwo2":
                ActionTwo2 = keyCode;
                break;
            default:
                Debug.Log("Wrong Key Entered");
                break;
        }

        encodeControls();
        StartCoroutine(waitBeforeUpdate());
        updateInputs();
    }

    public IEnumerator waitBeforeUpdate()
    {
        yield return new WaitForEndOfFrame();
        updateInputs();
    }
    public string getKey(string name)
    {
        switch (name)
        {
            case "Up1":
                Debug.Log("Up1 = " + Up1.ToString());
                return Up1.ToString();
            case "Left1":
                Debug.Log("Left1 = " + Left1.ToString());
                return Left1.ToString();
            case "Down1":
                return Down1.ToString();
            case "Right1":
                return Right1.ToString();
            case "ActionOne1":
                return ActionOne1.ToString();
            case "ActionTwo1":
                return ActionTwo1.ToString();
            case "Up2":
                return Up2.ToString();
            case "Left2":
                return Left2.ToString();
            case "Down2":
                return Down2.ToString();
            case "Right2":
                return Right2.ToString();
            case "ActionOne2":
                return ActionOne2.ToString();
            case "ActionTwo2":
                return ActionTwo2.ToString();
            default:
                return "ERROR";
        }
    }
}
