﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerExplosion : MonoBehaviour
{
    public GameObject Explosion;
    private bool hasExploded = false;
    private void OnCollisionEnter(Collision collision)
    {
        if (hasExploded != true)
        {
            hasExploded = true;
            Vector3 position = new Vector3(transform.position.x, 0.4f, transform.position.z);
            GameObject Explosion1 = Instantiate(Explosion, position, Quaternion.identity, null);
            Explosion1.SetActive(true);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "TeamOne" || collision.gameObject.tag == "TeamTwo")
        {
            collision.gameObject.GetComponent<BasicSoldierAI>().killSelf();
        }
    }
}
