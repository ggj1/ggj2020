﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCountdown : MonoBehaviour
{

    public float seconds = 5f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyMe());
    }

    public IEnumerator DestroyMe()
    {
        yield return new WaitForSeconds(seconds);
        if (gameObject.GetComponent<CapsuleCollider>())
        {
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
            yield return new WaitForSeconds(2f);
        }
        Destroy(gameObject);
    }
}
