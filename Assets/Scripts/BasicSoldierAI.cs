﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(BoxCollider))]

public class BasicSoldierAI : MonoBehaviour
{
	private NavMeshAgent navMesh;
	public GameManager gm;
	public GameObject deadbody;
	public GameObject ShootPrefab;

	[Header("Objectif")]
	public Vector3 target;

	[Header("Combat")]
	public string tagEnemy;
	public float distanceDetection = 1;
	public bool isItInContact;

	[Header("Stats")]
	public int aimPercentage = 10;
	public float attackDistance = 1f;
	public float reloadTime = 0.5f;

	public bool isFiring = false;

	private void Start()
	{

		gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		navMesh = gameObject.GetComponent<NavMeshAgent>();
		navMesh.stoppingDistance = attackDistance;
		navMesh.SetDestination(getTarget().transform.position);
	}

	public GameObject getTarget()
	{
		if (tagEnemy == "TeamOne")
		{
			if (gm.TeamOne.Count > 0)
			{
				return GetClosestEnemy(gm.TeamOne);
			}
			else
			{
				return GameObject.FindGameObjectWithTag("Nexus 1");
			}
		}
		else if (tagEnemy == "TeamTwo")
		{
			if (gm.TeamTwo.Count > 0)
			{
				return GetClosestEnemy(gm.TeamTwo);
			}
			else
			{
				return GameObject.FindGameObjectWithTag("Nexus 2");
			}
		}
		else
		{
			Debug.Log("This Object :" + gameObject + " has no target");
			return gameObject;
		}
	}

	public bool hasTouched()
	{
		if (Random.Range(0, 100) <= aimPercentage)
		{
			return true;
		}
		return false;
	}

	// Update is called once per frame
	void Update()
	{
		if (gm.isMenuAff)
		{
			navMesh.SetDestination(transform.position);
		}
		else
		{
			GameObject mechant = getTarget();
			navMesh.SetDestination(mechant.transform.position);
			if (isAtRange(mechant) && isFiring == false)
			{
				attack(mechant);
			}
		}
	}


	public bool isAtRange(GameObject mechant)
	{
		float dist = Vector3.Distance(mechant.transform.position, transform.position);
		if (dist < attackDistance)
		{
			return true;
		}
		return false;
	}

	public IEnumerator Reload()
	{
		yield return new WaitForSeconds(reloadTime);
		isFiring = false;
	}

	public void attack(GameObject mechant)
	{
		isFiring = true;
		if (ShootPrefab != null)
		{
			Debug.Log(gameObject.name+" is shooting Shooting on " + mechant.name);
			instantiateShoot(mechant);
		}
		StartCoroutine(Reload());
		if (mechant.tag == "TeamOne" && gameObject.tag != "TeamOne")
		{
			if (hasTouched())
			{
				gm.removeToListTeamOne(mechant);
			}
		}
		if (mechant.tag == "TeamTwo" && gameObject.tag != "TeamTwo")
		{
			if (hasTouched())
			{
				gm.removeToListTeamTwo(mechant);
			}
		}

		if (mechant.tag == "Nexus 1" && gameObject.tag != "Nexus 2")
		{
			if (hasTouched())
			{
				gm.verifLife(1);
			}
		}

		if (mechant.tag == "Nexus 2" && gameObject.tag != "Nexus 1")
		{
			if (hasTouched())
			{
				gm.verifLife(2);
			}
		}
	}

	public void instantiateShoot(GameObject mechant)
	{
		GameObject Shoot = Instantiate(ShootPrefab, transform.position, transform.rotation, null);
		Shoot.GetComponent<AimAt>().target = mechant.transform.position;
	}

	public void killSelf()
	{
		StartCoroutine(destroySelf());
	}

	public IEnumerator destroySelf()
	{
		yield return new WaitForEndOfFrame();
		if (deadbody) { Instantiate(deadbody, transform.position, transform.rotation, null); }
		Destroy(gameObject);

	}

	private GameObject GetClosestEnemy(List<GameObject> enemies)
	{
		GameObject tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (GameObject t in enemies)
		{
			if (t)
			{
				float dist = Vector3.Distance(t.transform.position, currentPos);
				if (dist < minDist)
				{
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}

}
