﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabDrop : MonoBehaviour
{
	public GameObject objectHold;
	public GameObject target;
	public bool isHolding;
	private bool lootzone = false;
	private KeyCode grab;
	private KeyCode drop;
	private Animator ani;
	private InputManager im;


	void Start()
    {
		im = GameObject.Find("InputManager").GetComponent<InputManager>();
		ani = transform.parent.GetComponent<ControllerChara>().Body.GetComponent<Animator>();

		objectHold = null;
	}

    void Update()
    {
		if (transform.parent.tag == "Player1")
		{
			if (Input.GetKeyDown(im.ActionOne1) && lootzone && !isHolding)
			{
				transform.parent.gameObject.GetComponent<ControllerChara>().enabled = false;
				target.GetComponentInChildren<PickItemMenu>().enabled = true;
				target.GetComponentInChildren<PickItemMenu>().activePlayer = transform.parent.gameObject;
				ani.SetBool("hold", true);
			}

			if (Input.GetKeyDown(im.ActionTwo1) && isHolding)
			{
				Drop();
				ani.SetBool("hold", false);
			}
		}
		else
		{
			if (Input.GetKeyDown(im.ActionOne2) && lootzone && !isHolding)
			{
				transform.parent.gameObject.GetComponent<ControllerChara>().enabled = false;
				target.GetComponentInChildren<PickItemMenu>().enabled = true;
				target.GetComponentInChildren<PickItemMenu>().activePlayer = transform.parent.gameObject;
				ani.SetBool("hold", true);
			}
			if (Input.GetKeyDown(im.ActionTwo2) && isHolding)
			{
				Drop();
				ani.SetBool("hold", false);
			}
		}
    }

	public void Drop()
	{
		isHolding = false;
		objectHold.GetComponent<Item>().grounded = true;
		objectHold.GetComponent<Item>().hold = false;
		objectHold.transform.parent= null;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("DeadBody"))
		{
			other.transform.GetChild(0).gameObject.SetActive(true);
			lootzone = true;
			target = other.gameObject;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("DeadBody"))
		{
			other.transform.GetChild(0).gameObject.SetActive(false);
			lootzone = false;
			target = null;
		}
	}
}
