﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class UIMO_Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject  focus;
    [SerializeField]
    private string NomSceneMachine1="Machine1";
    [SerializeField]
    private string NomSceneMachine2 = "Machine2";
    //Scene Machine1Scene;

    private string CurrentScene = "";
   // private ChangeSkyboxScript mychangesky;
    // Start is called before the first frame update
    void Start()
    {
       // mychangesky = GetComponent<ChangeSkyboxScript>();
        //charger le sousecran jeu
        SceneManager.LoadScene(NomSceneMachine1, LoadSceneMode.Additive);
        CurrentScene = NomSceneMachine1;
        // pour la culture Machine1Scene = SceneManager.GetSceneAt(0);
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name + " " + mode);
        string systemname = scene.name.Substring(0, 2);

        //récupérer la machine pour le focus,

        GameObject[] editorsonly = GameObject.FindGameObjectsWithTag("EditorOnly");
        foreach (GameObject go in editorsonly)
        {
            go.SetActive(false);
        }
        GameObject machine = GameObject.FindGameObjectWithTag("Player");
        ConnectFocusTo(machine);
    }
    
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void ConnectFocusTo(GameObject go)
    {
        if (focus.transform.childCount>0)
        {
            foreach (Transform child in focus.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        if (!go) return;
        go.transform.parent = focus.transform ;
    }

    bool machineison = false;
    public  void StartMachine()
    {
        machineison = !machineison;
       // focus.transform.GetComponentInChildren<PumpManager>().startStop = true;
    }
    public void SetDecor1()
    {
        if (machineison) StartMachine();
        if (CurrentScene != NomSceneMachine1)
        { LoadMyMachine(NomSceneMachine1); }
       // mychangesky.SetDay();

    }
    public void SetDecor2()
    {
        if (machineison) StartMachine();
        if (CurrentScene != NomSceneMachine2)
        { LoadMyMachine(NomSceneMachine2); }
       // mychangesky.SetNight();
    }

    private void LoadMyMachine(string MachineName)
    {
        if ( CurrentScene != MachineName)
        {
            //retirer les sous scenes
            if (focus.transform.childCount > 0)
            {
                foreach (Transform child in focus.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
            StartCoroutine(StartActivatePlayer(MachineName));
        }
        

    }

    IEnumerator StartActivatePlayer(string MachineName)
    {
        AsyncOperation ao = SceneManager.UnloadSceneAsync(CurrentScene);
        yield return ao;
        SceneManager.LoadScene(MachineName, LoadSceneMode.Additive);
        CurrentScene = MachineName;
        

    }


}
