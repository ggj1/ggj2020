﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MO_TerrainManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SetHDRI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    public Cubemap nightSkyReflection;
    public Material skyboxNight;
   

    public void SetHDRI()
    {
        if (nightSkyReflection != null)
            RenderSettings.customReflection = nightSkyReflection;
        if (skyboxNight != null)
            RenderSettings.skybox = skyboxNight;

        RenderSettings.ambientIntensity = 2.666284f;
        GameObject[] editorsonly = GameObject.FindGameObjectsWithTag("EditorOnly");
        foreach (GameObject go in editorsonly)
        {
            go.SetActive(false);
        }

    }
}
